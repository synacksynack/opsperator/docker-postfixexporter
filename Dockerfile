FROM docker.io/golang:1.13 AS builder

# Postfix Exporter image for OpenShift Origin

WORKDIR /src

RUN apt-get update -qq \
    && apt-get install -qqy build-essential libsystemd-dev

COPY go.mod go.sum ./
RUN go mod download
RUN go mod verify

COPY . .

ENV GO111MODULE=on
RUN go test
RUN go build -o /bin/postfix_exporter

FROM docker.io/debian:buster-slim

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive

LABEL io.k8s.description="Postfix Prometheus Exporter Image." \
      io.k8s.display-name="Postfix Prometheus Exporter" \
      io.openshift.expose-services="9154:http" \
      io.openshift.tags="prometheus,exporter,postfix" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-postfixexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.0"

WORKDIR /

RUN set -x \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && apt-get -y install dumb-init curl \
    && echo "# Cleaning Up" \
    && apt-get -y remove --purge gnupg build-essential cpanminus \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rvf /usr/share/man /usr/share/doc /var/lib/apt/lists/* \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

COPY --from=builder /bin/postfix_exporter /bin/
ENTRYPOINT ["dumb-init","--","/bin/postfix_exporter"]
